CREATE TABLE IF NOT EXISTS character_subclasses (
  charId INT  NOT NULL DEFAULT 0,
  class_id int NOT NULL DEFAULT 0,
  exp decimal(20,0) NOT NULL DEFAULT 0,
  sp decimal(11,0) NOT NULL DEFAULT 0,
  level int NOT NULL DEFAULT 40,
  class_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,class_id)
) ;
