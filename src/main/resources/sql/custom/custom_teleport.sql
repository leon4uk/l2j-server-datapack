CREATE TABLE IF NOT EXISTS custom_teleport (
  Description varchar(75) DEFAULT NULL,
  id int  NOT NULL DEFAULT 0,
  loc_x int DEFAULT NULL,
  loc_y int DEFAULT NULL,
  loc_z int DEFAULT NULL,
  price int  DEFAULT NULL,
  fornoble int NOT NULL DEFAULT 0,
  itemId int  NOT NULL DEFAULT '57',
  PRIMARY KEY (id)
) ;
