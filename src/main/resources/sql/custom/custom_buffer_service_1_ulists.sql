CREATE TABLE IF NOT EXISTS custom_buffer_service_ulists (
  ulist_id int  NOT NULL ,
  ulist_char_id int unique NOT NULL,
  ulist_name varchar(255) unique NOT NULL,
  PRIMARY KEY (ulist_id),
  CONSTRAINT custom_buffer_service_ulists_ibfk_1 FOREIGN KEY (ulist_char_id) REFERENCES characters (charId) ON DELETE CASCADE ON UPDATE CASCADE
);
