CREATE TABLE IF NOT EXISTS custom_spawnlist (
  location varchar(40) NOT NULL DEFAULT '',
  count int  NOT NULL DEFAULT 0,
  npc_templateid int  NOT NULL DEFAULT 0,
  locx int NOT NULL DEFAULT 0,
  locy int NOT NULL DEFAULT 0,
  locz int NOT NULL DEFAULT 0,
  randomx int NOT NULL DEFAULT 0,
  randomy int NOT NULL DEFAULT 0,
  heading int NOT NULL DEFAULT 0,
  respawn_delay int NOT NULL DEFAULT 0,
  respawn_random int NOT NULL DEFAULT 0,
  loc_id int NOT NULL DEFAULT 0,
  periodOfDay int  NOT NULL DEFAULT 0
) ;
