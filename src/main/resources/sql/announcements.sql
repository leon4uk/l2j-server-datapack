CREATE TABLE IF NOT EXISTS announcements (
  id int  NOT NULL ,
  type int NOT NULL,
  initial bigint NOT NULL DEFAULT 0,
  delay bigint NOT NULL DEFAULT 0,
  repeat int NOT NULL DEFAULT 0,
  author text NOT NULL,
  content text NOT NULL,
  PRIMARY KEY (id)
) ;

INSERT INTO announcements (id, type, author, content) VALUES
(1, 0, 'L2JServer', 'Thanks for using our server.'),
(2, 1, 'L2JServer', 'https://lineage.yourapi.ru/');
