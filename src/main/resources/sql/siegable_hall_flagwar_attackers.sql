CREATE TABLE IF NOT EXISTS siegable_hall_flagwar_attackers (
  hall_id int  NOT NULL DEFAULT 0,
  flag int  NOT NULL DEFAULT 0,
  npc int  NOT NULL DEFAULT 0,
  clan_id int  NOT NULL DEFAULT 0,
  PRIMARY KEY (flag),
  KEY hall_id (hall_id),
  KEY clan_id (clan_id)
) ;
