CREATE TABLE IF NOT EXISTS character_ui_actions (
  charId int  NOT NULL DEFAULT 0,
  cat int NOT NULL,
  orderi int NOT NULL,
  cmd int NOT NULL DEFAULT 0,
  key int NOT NULL,
  tgKey1 int DEFAULT NULL,
  tgKey2 int DEFAULT NULL,
  show int NOT NULL,
  PRIMARY KEY (charId,cat,cmd)
);
