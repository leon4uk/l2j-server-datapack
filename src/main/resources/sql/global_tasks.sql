CREATE TABLE IF NOT EXISTS global_tasks (
  id int NOT NULL,
  task varchar(50) NOT NULL DEFAULT '',
  type varchar(50) NOT NULL DEFAULT '',
  last_activation bigint  NOT NULL DEFAULT 0,
  param1 varchar(100) NOT NULL DEFAULT '',
  param2 varchar(100) NOT NULL DEFAULT '',
  param3 varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
) ;
