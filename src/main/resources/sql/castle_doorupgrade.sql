CREATE TABLE IF NOT EXISTS castle_doorupgrade (
  doorId int  NOT NULL DEFAULT 0,
  ratio int  NOT NULL DEFAULT 0,
  castleId int  NOT NULL DEFAULT 0,
  PRIMARY KEY (doorId)
) ;
