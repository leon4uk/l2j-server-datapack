CREATE TABLE IF NOT EXISTS character_pet_skills_save (
  petObjItemId INT NOT NULL DEFAULT 0,
  skill_id INT NOT NULL DEFAULT 0,
  skill_level int NOT NULL DEFAULT 1,
  remaining_time INT NOT NULL DEFAULT 0,
  buff_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (petObjItemId,skill_id,skill_level)
) ;
