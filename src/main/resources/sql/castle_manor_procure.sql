CREATE TABLE IF NOT EXISTS castle_manor_procure (
 castle_id int  NOT NULL DEFAULT 0,
 crop_id int  NOT NULL DEFAULT 0,
 amount int  NOT NULL DEFAULT 0,
 start_amount int  NOT NULL DEFAULT 0,
 price int  NOT NULL DEFAULT 0,
 reward_type int  NOT NULL DEFAULT 0,
 next_period int  NOT NULL DEFAULT 1,
  PRIMARY KEY (castle_id,crop_id,next_period)
) ;
