CREATE TABLE IF NOT EXISTS character_offline_trade_items (
  charId int  NOT NULL,
  item int  NOT NULL DEFAULT 0, -- itemId(for buy) & ObjectId(for sell)
  count bigint  NOT NULL DEFAULT 0,
  price bigint  NOT NULL DEFAULT 0,
  KEY charId (charId),
  KEY item (item)
) ;
