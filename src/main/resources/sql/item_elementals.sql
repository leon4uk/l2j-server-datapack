CREATE TABLE IF NOT EXISTS item_elementals (
  itemId int NOT NULL DEFAULT 0,
  elemType int NOT NULL DEFAULT -1,
  elemValue int NOT NULL DEFAULT -1,
  PRIMARY KEY (itemId, elemType)
) ;
