CREATE TABLE IF NOT EXISTS olympiad_nobles_eom (
  charId int  NOT NULL DEFAULT 0,
  class_id int  NOT NULL DEFAULT 0,
  olympiad_points int  NOT NULL DEFAULT 0,
  competitions_done int  NOT NULL DEFAULT 0,
  competitions_won int  NOT NULL DEFAULT 0,
  competitions_lost int  NOT NULL DEFAULT 0,
  competitions_drawn int  NOT NULL DEFAULT 0,
  PRIMARY KEY (charId)
) ;
