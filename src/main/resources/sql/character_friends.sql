CREATE TABLE IF NOT EXISTS character_friends (
  charId INT  NOT NULL DEFAULT 0,
  friendId INT  NOT NULL DEFAULT 0,
  relation INT  NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,friendId)
) ;
