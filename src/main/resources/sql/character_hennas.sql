CREATE TABLE IF NOT EXISTS character_hennas (
  charId INT  NOT NULL DEFAULT 0,
  symbol_id INT,
  slot INT NOT NULL DEFAULT 0,
  class_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,slot,class_index)
) ;
