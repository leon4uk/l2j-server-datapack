CREATE TABLE IF NOT EXISTS character_item_reuse_save (
  charId INT NOT NULL DEFAULT 0,
  itemId INT NOT NULL DEFAULT 0,
  itemObjId int NOT NULL DEFAULT 1,
  reuseDelay int NOT NULL DEFAULT 0,
  systime BIGINT  NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,itemId,itemObjId)
) ;
