CREATE TABLE IF NOT EXISTS character_ui_categories (
  charId int  NOT NULL DEFAULT 0,
  catId int NOT NULL,
  orderi int NOT NULL,
  cmdId int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,catId,orderi)
) ;
