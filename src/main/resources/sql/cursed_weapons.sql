CREATE TABLE IF NOT EXISTS cursed_weapons (
  itemId INT,
  charId INT  NOT NULL DEFAULT 0,
  playerKarma INT DEFAULT 0,
  playerPkKills INT DEFAULT 0,
  nbKills INT DEFAULT 0,
  endTime bigint  NOT NULL DEFAULT 0,
  PRIMARY KEY (itemId),
  KEY charId (charId)
) ;
