CREATE TABLE IF NOT EXISTS item_auction (
  auctionId int NOT NULL,
  instanceId int NOT NULL,
  auctionItemId int NOT NULL,
  startingTime bigint  NOT NULL DEFAULT 0,
  endingTime bigint  NOT NULL DEFAULT 0,
  auctionStateId int NOT NULL,
  PRIMARY KEY (auctionId)
) ;
