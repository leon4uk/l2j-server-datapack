CREATE TABLE IF NOT EXISTS auction (
  id int NOT NULL DEFAULT 0,
  sellerId int NOT NULL DEFAULT 0,
  sellerName varchar(50) NOT NULL DEFAULT 'NPC',
  sellerClanName varchar(50) NOT NULL DEFAULT '',
  itemType varchar(25) NOT NULL DEFAULT '',
  itemId int NOT NULL DEFAULT 0,
  itemObjectId int NOT NULL DEFAULT 0,
  itemName varchar(40) NOT NULL DEFAULT '',
  itemQuantity BIGINT  NOT NULL DEFAULT 0,
  startingBid BIGINT  NOT NULL DEFAULT 0,
  currentBid BIGINT  NOT NULL DEFAULT 0,
  endDate bigint  NOT NULL DEFAULT 0,
  PRIMARY KEY (itemType,itemId,itemObjectId),
  KEY id (id)
) ;
