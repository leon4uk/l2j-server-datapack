CREATE TABLE IF NOT EXISTS heroes_diary (
  charId int  NOT NULL,
  time bigint  NOT NULL DEFAULT 0,
  action int  NOT NULL DEFAULT 0,
  param int  NOT NULL DEFAULT 0,
  KEY charId (charId)
) ;
