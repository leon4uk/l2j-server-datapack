CREATE TABLE IF NOT EXISTS character_quests (
  charId INT  NOT NULL DEFAULT 0,
  name VARCHAR(60) NOT NULL DEFAULT '',
  var  VARCHAR(20) NOT NULL DEFAULT '',
  value VARCHAR(255) ,
  class_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,name,var,class_index)
) ;
