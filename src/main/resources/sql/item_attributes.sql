CREATE TABLE IF NOT EXISTS item_attributes (
  itemId int NOT NULL DEFAULT 0,
  augAttributes int NOT NULL DEFAULT -1,
  PRIMARY KEY (itemId)
) ;
