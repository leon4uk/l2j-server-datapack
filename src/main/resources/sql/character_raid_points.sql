CREATE TABLE IF NOT EXISTS character_raid_points (
  charId INT  NOT NULL DEFAULT 0,
  boss_id INT  NOT NULL DEFAULT 0,
  points INT  NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,boss_id)
) ;
