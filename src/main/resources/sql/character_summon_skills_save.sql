CREATE TABLE IF NOT EXISTS character_summon_skills_save (
  ownerId INT NOT NULL DEFAULT 0,
  ownerClassIndex int NOT NULL DEFAULT 0,
  summonSkillId INT NOT NULL DEFAULT 0,
  skill_id INT NOT NULL DEFAULT 0,
  skill_level int NOT NULL DEFAULT 1,
  remaining_time INT NOT NULL DEFAULT 0,
  buff_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (ownerId,ownerClassIndex,summonSkillId,skill_id,skill_level)
) ;
