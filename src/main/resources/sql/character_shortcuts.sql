CREATE TABLE IF NOT EXISTS character_shortcuts (
  charId INT  NOT NULL DEFAULT 0,
  slot decimal(3) NOT NULL DEFAULT 0,
  page decimal(3) NOT NULL DEFAULT 0,
  type decimal(3) ,
  shortcut_id decimal(16) ,
  level varchar(4) ,
  class_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,slot,page,class_index)
) ;
