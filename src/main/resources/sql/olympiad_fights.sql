CREATE TABLE IF NOT EXISTS olympiad_fights (
  charOneId int  NOT NULL,
  charTwoId int  NOT NULL,
  charOneClass int  NOT NULL DEFAULT 0,
  charTwoClass int  NOT NULL DEFAULT 0,
  winner int  NOT NULL DEFAULT 0,
  start bigint  NOT NULL DEFAULT 0,
  time bigint  NOT NULL DEFAULT 0,
  classed int  NOT NULL DEFAULT 0,
  KEY charOneId (charOneId),
  KEY charTwoId (charTwoId)
) ;
