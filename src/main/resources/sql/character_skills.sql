CREATE TABLE IF NOT EXISTS character_skills (
  charId INT  NOT NULL DEFAULT 0,
  skill_id INT NOT NULL DEFAULT 0,
  skill_level int NOT NULL DEFAULT 1,
  class_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,skill_id,class_index)
) ;
