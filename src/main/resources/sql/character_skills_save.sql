CREATE TABLE IF NOT EXISTS character_skills_save (
  charId INT NOT NULL DEFAULT 0,
  skill_id INT NOT NULL DEFAULT 0,
  skill_level int NOT NULL DEFAULT 1,
  remaining_time INT NOT NULL DEFAULT 0,
  reuse_delay int NOT NULL DEFAULT 0,
  systime bigint  NOT NULL DEFAULT 0,
  restore_type int NOT NULL DEFAULT 0,
  class_index int NOT NULL DEFAULT 0,
  buff_index int NOT NULL DEFAULT 0,
  PRIMARY KEY (charId,skill_id,skill_level,class_index)
) ;
