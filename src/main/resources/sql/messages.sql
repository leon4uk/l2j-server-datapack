CREATE TABLE IF NOT EXISTS messages (
  messageId INT NOT NULL DEFAULT 0,
  senderId INT NOT NULL DEFAULT 0,
  receiverId INT NOT NULL DEFAULT 0,
  subject text,
  content TEXT,
  expiration bigint  NOT NULL DEFAULT 0,
  reqAdena BIGINT NOT NULL DEFAULT 0,
  hasAttachments boolean DEFAULT 'false' NOT NULL,
  isUnread boolean DEFAULT 'true' NOT NULL,
  isDeletedBySender boolean DEFAULT 'false' NOT NULL,
  isDeletedByReceiver boolean DEFAULT 'false' NOT NULL,
  isLocked boolean DEFAULT 'false' NOT NULL,
  sendBySystem int NOT NULL DEFAULT 0,
  isReturned boolean DEFAULT 'false' NOT NULL,
  PRIMARY KEY (messageId)
) ;
