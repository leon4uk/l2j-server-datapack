CREATE TABLE IF NOT EXISTS character_recipebook (
  charId INT  NOT NULL DEFAULT 0,
  id decimal(11) NOT NULL DEFAULT 0,
  classIndex int NOT NULL DEFAULT 0,
  type INT NOT NULL DEFAULT 0,
  PRIMARY KEY (id,charId,classIndex)
) ;
