CREATE TABLE IF NOT EXISTS pets (
  item_obj_id int  NOT NULL,
  name varchar(16),
  level int  NOT NULL,
  curHp int  DEFAULT 0,
  curMp int  DEFAULT 0,
  exp bigint  DEFAULT 0,
  sp int  DEFAULT 0,
  fed int  DEFAULT 0,
  ownerId int NOT NULL DEFAULT 0,
  restore boolean NOT NULL DEFAULT 'false',
  PRIMARY KEY (item_obj_id),
  KEY ownerId (ownerId)
) ;
