CREATE TABLE IF NOT EXISTS character_premium_items (
  charId int NOT NULL,
  itemNum int NOT NULL,
  itemId int NOT NULL,
  itemCount bigint  NOT NULL,
  itemSender varchar(50) NOT NULL
) ;
