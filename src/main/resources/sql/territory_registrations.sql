CREATE TABLE IF NOT EXISTS territory_registrations (
  castleId int NOT NULL DEFAULT 0,
  registeredId int NOT NULL DEFAULT 0,
  PRIMARY KEY (castleId,registeredId)
) ;
