CREATE TABLE IF NOT EXISTS crests (
	crest_id INT,
	data bit(2176) NOT NULL,
	type int NOT NULL,
	PRIMARY KEY(crest_id)
);
