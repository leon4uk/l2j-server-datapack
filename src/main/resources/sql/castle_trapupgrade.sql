CREATE TABLE IF NOT EXISTS castle_trapupgrade (
  castleId int  NOT NULL DEFAULT 0,
  towerIndex int  NOT NULL DEFAULT 0,
  level int  NOT NULL DEFAULT 0,
  PRIMARY KEY (towerIndex,castleId)
) ;
