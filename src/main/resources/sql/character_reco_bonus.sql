CREATE TABLE IF NOT EXISTS character_reco_bonus (
  charId int unique NOT NULL,
  rec_have int  NOT NULL DEFAULT 0,
  rec_left int  NOT NULL DEFAULT 0,
  time_left bigint  NOT NULL DEFAULT 0
) ;
