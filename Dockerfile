FROM openjdk:14-alpine

COPY . /opt/l2j/server/game/l2j-server-datapack
WORKDIR /opt/l2j/server/game/l2j-server-datapack

RUN apk update \
    && apk --no-cache add unzip maven\
    && cd /opt/l2j/server/game/l2j-server-datapack && mvn install \
    && unzip /opt/l2j/server/game/l2j-server-datapack/target/*.zip -d /opt/l2j/server/game/l2j-server-datapack \
    && rm -rf /opt/l2j/server/game/l2j-server-datapack/target/ && apk del unzip maven
